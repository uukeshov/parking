package kg.parking.service.crud.impl;

import kg.parking.model.base.BaseEntity;
import kg.parking.service.crud.CrudService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@Transactional
public class DefaultCrudServiceImpl<T extends BaseEntity> implements CrudService<T> {

    private final CrudRepository<T, Long> crudRepository;

    public DefaultCrudServiceImpl(CrudRepository<T, Long> crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public T get(@NotNull Long id) {
        return crudRepository.findById(id).orElse(null);
    }

    @Override
    public T saveOrUpdate(T entity) {
        return internalSaveOrUpdate(entity);
    }

    @Override
    public List<T> saveOrUpdate(List<T> entities) {
        List<T> result = new LinkedList<>();
        for (T entity : entities) {
            result.add(internalSaveOrUpdate(entity));
        }
        return result;
    }

    @Override
    public void remove(@NotNull Long id) {
        crudRepository.deleteById(id);
    }

    private T internalSaveOrUpdate(@NotNull T entity) {
        if (null == entity.getId()) {
            entity = crudRepository.save(entity);
            return entity;
        }
        return crudRepository.save(entity);
    }
}
