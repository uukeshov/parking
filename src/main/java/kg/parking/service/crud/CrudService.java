package kg.parking.service.crud;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface CrudService<T> {
    T get(@NotNull Long id);

    T saveOrUpdate(@NotNull T entity);

    List<T> saveOrUpdate(@NotNull List<T> entity);

    void remove(@NotNull Long id);
}
