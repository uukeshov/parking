package kg.parking.service.parking.impl;

/* 
    project name: parking
    user: uukeshov
    time: 01.12.2020 21:55
 */

import kg.parking.dto.ParkingDto;
import kg.parking.model.common.Parking;
import kg.parking.model.common.ParkingRepository;
import kg.parking.service.crud.impl.DefaultCrudServiceImpl;
import kg.parking.service.parking.ParkingManageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DefaultParkingManageServiceImpl extends DefaultCrudServiceImpl<Parking> implements ParkingManageService {

    private final ParkingRepository parkingRepository;

    @Value("${parking.size}")
    public int parkingSize;

    public DefaultParkingManageServiceImpl(CrudRepository<Parking, Long> crudRepository, ParkingRepository parkingRepository) {
        super(crudRepository);
        this.parkingRepository = parkingRepository;
    }

    @Override
    public int getFreePlace() {

        int allResevedPlaces = parkingRepository.findAll().size();

        if (allResevedPlaces >= parkingSize) {
            return 0;
        }

        return parkingSize - allResevedPlaces;
    }

    @Override
    public List<ParkingDto> getAll() {
        return parkingRepository.findAll().stream().map(parking -> ParkingDto.builder().carBodyType(parking.getCarBodyType()).carNumber(parking.getCarNumber()).build()).collect(Collectors.toList());
    }

    @Override
    public boolean reserve(ParkingDto parkingDto) {
        Parking parking = Parking.builder().carBodyType(parkingDto.getCarBodyType()).carNumber(parkingDto.getCarNumber()).build();

        try {
            if (getFreePlace() > 0) {
                saveOrUpdate(parking);
                return true;
            }
        } catch (Exception exception) {
            exception.getMessage();
        }

        return false;
    }

    @Override
    public boolean release(String carNumber) {
        Optional<Parking> parking = parkingRepository.findByCarNumber(carNumber);

        if (parking.isPresent()) {
            remove(parking.get().getId());
            return true;
        } else {
            return false;
        }
    }
}
