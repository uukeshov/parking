package kg.parking.service.parking;

/* 
    project name: parking
    user: uukeshov
    time: 01.12.2020 21:51
 */

import kg.parking.dto.ParkingDto;

import java.util.List;

public interface ParkingManageService {
    int getFreePlace();

    List<ParkingDto> getAll();

    boolean reserve(ParkingDto parkingDto);

    boolean release(String carNumber);
}
