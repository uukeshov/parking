package kg.parking.common.enums;

/*
    project name: main
    user: uukeshov
    time: 08.10.2020 17:17
 */

import lombok.Getter;

public enum RequestErrors {
    SUCCESS("SUCCESS"),
    ERROR("ERROR");

    @Getter
    private final String status;

    RequestErrors(String status) {
        this.status = status;
    }
}
