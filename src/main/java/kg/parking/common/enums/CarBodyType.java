package kg.parking.common.enums;

/* 
    project name: parking
    user: uukeshov
    time: 01.12.2020 22:14
 */

public enum CarBodyType {
    SEDAN(1),
    LIMUZIN(2),
    TRACK(3),
    BIKE(1);

    private final int value;

    CarBodyType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
