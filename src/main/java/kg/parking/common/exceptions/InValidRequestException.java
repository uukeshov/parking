package kg.parking.common.exceptions;

public class InValidRequestException extends RuntimeException {
    public InValidRequestException(String errorMessage) {
        super(errorMessage);
    }
}
