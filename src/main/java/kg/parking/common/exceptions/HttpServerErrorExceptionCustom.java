package kg.parking.common.exceptions;

/* 
    project name: core
    user: uukeshov
    time: 21.07.2020 20:52
 */

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;

public class HttpServerErrorExceptionCustom extends HttpStatusCodeException {
    protected HttpServerErrorExceptionCustom(HttpStatus statusCode, String statusText) {
        super(statusCode, statusText);
    }
}
