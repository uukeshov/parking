package kg.parking.common.exceptions;

public class ErrorHandlerException extends RuntimeException {
    public ErrorHandlerException(String errorMessage) {
        super(errorMessage);
    }
}
