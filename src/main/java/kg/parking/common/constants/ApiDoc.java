package kg.parking.common.constants;

public class ApiDoc {
    public static final String UTF8_JSON = "application/json; charset=utf-8";
    public static final String UTF8_XML = "text/xml; charset=utf-8";
}
