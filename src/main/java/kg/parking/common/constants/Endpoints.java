package kg.parking.common.constants;

public class Endpoints {
    private static final String API = "/api";
    private static final String V1 = "/v1";

    public static final String MANAGEMENT = API + V1 + "/management";
}
