package kg.parking.controller.advice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@Data
public class ErrorResponseDto {
    private LocalDateTime dateTime;
    private String error;
    private String message;
}
