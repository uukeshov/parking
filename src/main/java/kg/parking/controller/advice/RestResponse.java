package kg.parking.controller.advice;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Builder
@AllArgsConstructor
@Data
public class RestResponse<E> {

    @JsonIgnore
    private HttpStatus httpStatus;
    private E response;
    private Integer status;
    private Map<String, String> errors;

    public void addErrors(String key, String value) {
        this.errors.put(key, value);
    }
}
