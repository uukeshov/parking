package kg.parking.controller.advice;

import kg.parking.common.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public class ControllerAdvice {

    private static final String MESSAGE_KEY = "message";

    private RestResponse<?> createRestResponse(String message, HttpStatus httpStatus) {
        RestResponse response = RestResponse.builder().response(MESSAGE_KEY).httpStatus(httpStatus).errors(new HashMap<>()).build();
        response.addErrors(MESSAGE_KEY, message);
        return response;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ObjectNotFoundException.class)
    public RestResponse<?> handleObjectNotFoundException(ObjectNotFoundException ex) {
        return createRestResponse(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnauthorizedException.class)
    public RestResponse<?> handleUnauthorizedException(UnauthorizedException ex) {
        return createRestResponse(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(InValidRequestException.class)
    public RestResponse<?> handleInValidRequestException(InValidRequestException ex) {
        return createRestResponse(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(ErrorHandlerException.class)
    public RestResponse<?> handleErrorHandlerException(ErrorHandlerException ex) {
        return createRestResponse(ex.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(HttpServerErrorExceptionCustom.class)
    public RestResponse<?> handleHttpServerErrorExceptionCustom(HttpServerErrorExceptionCustom ex) {
        return createRestResponse(ex.getMessage(), ex.getStatusCode());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public RestResponse<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            return createRestResponse(error.getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        return createRestResponse(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);

    }
}
