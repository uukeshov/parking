package kg.parking.controller;

/* 
    project name: parking
    user: uukeshov
    time: 01.12.2020 20:26
 */

import io.swagger.annotations.ApiOperation;
import kg.parking.common.constants.ApiDoc;
import kg.parking.common.enums.RequestErrors;
import kg.parking.controller.advice.ErrorResponseDto;
import kg.parking.controller.advice.RestResponse;
import kg.parking.dto.ParkingDto;
import kg.parking.service.parking.ParkingManageService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Map;

import static kg.parking.common.constants.Endpoints.MANAGEMENT;

@CrossOrigin
@RestController
@RequestMapping(value = MANAGEMENT)
@AllArgsConstructor
public class MainController {

    private final ParkingManageService parkingManageService;

    @ApiOperation(value = "Получение списка всех машин в парковке")
    @GetMapping(value = "getAllCars", produces = ApiDoc.UTF8_JSON)
    public RestResponse<?> getAllCars() {
        try {

            return RestResponse.builder()
                    .status(0)
                    .response(parkingManageService.getAll())
                    .httpStatus(HttpStatus.OK)
                    .errors(Map.of("1", RequestErrors.SUCCESS.name()))
                    .build();
        } catch (Exception e) {
            ErrorResponseDto errorResponseDto = ErrorResponseDto
                    .builder()
                    .dateTime(LocalDateTime.now())
                    .error(e.getMessage())
                    .message("Error happened!")
                    .build();
            return RestResponse.builder()
                    .status(-1)
                    .response(errorResponseDto)
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .errors(Map.of("1", e.getMessage()))
                    .build();
        }
    }

    @ApiOperation(value = "Получение информации о наличии свободного места")
    @GetMapping(value = "getFreePlace", produces = ApiDoc.UTF8_JSON)
    public RestResponse<?> getFreePlace() {
        try {

            return RestResponse.builder()
                    .status(0)
                    .response(parkingManageService.getFreePlace())
                    .httpStatus(HttpStatus.OK)
                    .errors(Map.of("1", RequestErrors.SUCCESS.name()))
                    .build();
        } catch (Exception e) {
            ErrorResponseDto errorResponseDto = ErrorResponseDto
                    .builder()
                    .dateTime(LocalDateTime.now())
                    .error(e.getMessage())
                    .message("Нет свободных мест!")
                    .build();
            return RestResponse.builder()
                    .status(-1)
                    .response(errorResponseDto)
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .errors(Map.of("1", e.getMessage()))
                    .build();
        }
    }

    @ApiOperation(value = "Парковка машины")
    @PostMapping(value = "reserve", produces = ApiDoc.UTF8_JSON)
    public RestResponse<?> reserve(@RequestBody @Valid ParkingDto parkingDto) {
        try {
            return RestResponse.builder()
                    .status(0)
                    .response(parkingManageService.reserve(parkingDto))
                    .httpStatus(HttpStatus.OK)
                    .errors(Map.of("1", RequestErrors.SUCCESS.name()))
                    .build();
        } catch (Exception e) {
            ErrorResponseDto errorResponseDto = ErrorResponseDto
                    .builder()
                    .dateTime(LocalDateTime.now())
                    .error(e.getMessage())
                    .message("Error happened!")
                    .build();
            return RestResponse.builder()
                    .status(-1)
                    .response(errorResponseDto)
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .errors(Map.of("1", e.getMessage()))
                    .build();
        }
    }

    @ApiOperation(value = "Выезд машины из парковки")
    @DeleteMapping(value = "release/{carNumber}", produces = ApiDoc.UTF8_JSON)
    public RestResponse<?> release(@PathVariable String carNumber) {
        try {
            return RestResponse.builder()
                    .status(0)
                    .response(parkingManageService.release(carNumber))
                    .httpStatus(HttpStatus.OK)
                    .errors(Map.of("1", RequestErrors.SUCCESS.name()))
                    .build();
        } catch (Exception e) {
            ErrorResponseDto errorResponseDto = ErrorResponseDto
                    .builder()
                    .dateTime(LocalDateTime.now())
                    .error(e.getMessage())
                    .message("Error happened!")
                    .build();
            return RestResponse.builder()
                    .status(-1)
                    .response(errorResponseDto)
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .errors(Map.of("1", e.getMessage()))
                    .build();
        }
    }
}
