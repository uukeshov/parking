package kg.parking.dto;

/* 
    project name: parking
    user: uukeshov
    time: 01.12.2020 22:07
 */

import kg.parking.common.enums.CarBodyType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
@AllArgsConstructor
public class ParkingDto {
    @NonNull
    private CarBodyType carBodyType;
    @NonNull
    private String carNumber;

}
