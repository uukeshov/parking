package kg.parking.model.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
@EqualsAndHashCode(callSuper = true)
public abstract class BaseLoggedEntity extends BaseEntity {

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @PrePersist
    public void prePersistLog() {
        createdBy = "system"; //SecurityContextUtil.getCurrentUserId();
        creationDate = LocalDateTime.now();
    }

}
