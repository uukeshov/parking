package kg.parking.model.common;

import kg.parking.common.enums.CarBodyType;
import kg.parking.model.base.BaseLoggedEntity;
import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Builder
@Table(name = "parking")
public class Parking extends BaseLoggedEntity {
    @NonNull
    @Column(name = "car_number", length = 50, unique = true, nullable = false)
    private String carNumber;

    @NonNull
    @Column(name = "car_body_type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private CarBodyType carBodyType;
}
