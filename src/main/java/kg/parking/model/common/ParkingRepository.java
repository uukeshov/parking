package kg.parking.model.common;

/* 
    project name: parking
    user: uukeshov
    time: 01.12.2020 21:56
 */

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ParkingRepository extends JpaRepository<Parking, Long> {
    Optional<Parking> findByCarNumber(String carNumber);
}
