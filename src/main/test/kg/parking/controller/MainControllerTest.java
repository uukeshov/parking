package kg.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kg.parking.common.enums.CarBodyType;
import kg.parking.dto.ParkingDto;
import kg.parking.service.parking.ParkingManageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
/* 
    project name: parking
    user: uukeshov
    time: 01.12.2020 23:52
 */

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.yml")
public class MainControllerTest {

    @Autowired
    private MockMvc mockMvc;

//    @Autowired
//    private MainController controller;

    @Autowired
    private ParkingManageService parkingManageService;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    public void getFreePlace() throws Exception {
        parkingManageService.reserve(ParkingDto.builder().carBodyType(CarBodyType.BIKE).carNumber("test").build());

        mockMvc.perform(get("/api/v1/management/getFreePlace")
                .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("199")));
    }

//    @Test
//    public void getAllCars() throws Exception {
//        mockMvc.perform(get("/api/v1/management/getAllCars")
//                .contentType("application/json"))
//                .andExpect(status().isOk());
//    }

//    @Test
//    public void reserv() throws Exception {
//        ParkingDto carNumber = ParkingDto.builder().carNumber("123").carBodyType(CarBodyType.BIKE).build();
//        mockMvc.perform(post("/api/v1/management/reserv")
//                .contentType("application/json")
//                .content(objectMapper.writeValueAsString(carNumber)))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void release() throws Exception {
//        mockMvc.perform(delete("/api/v1/management/release")
//                .contentType("application/json")
//                .param("carNumber", "123")
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isNotFound());
//    }


}